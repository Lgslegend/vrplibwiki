/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/




/* VRP_scenario +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	See VRP_scenario.h for description

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/

#include <iostream>
#include <cstdlib>	// rand	
#include <limits>
#include <vector>
#include <algorithm>
#include <random>
// #include <cmath>
#include "VRP_instance.h"
#include "VRP_scenario.h"
#include "VRP_solution.h"
#include "tools.h"

using namespace std;



// Scenario_SS_DS_VRPTW_CR::Scenario_SS_DS_VRPTW_CR(const VRP_instance& instance, VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR>* pool) : I(instance) {
Scenario_SS_DS_VRPTW_CR::Scenario_SS_DS_VRPTW_CR(const VRP_instance& instance, VRP_type vrp_type, int currentTime) : I(instance) {
	ASSERT(instance.getHorizon() < numeric_limits<int>::max(), "horizon size isn't set !");
	sol_type = vrp_type;
	reSampleScenario(currentTime);
	// this->pool_volatile = pool;
}

Scenario_SS_DS_VRPTW_CR::~Scenario_SS_DS_VRPTW_CR() {
	if (s_copy)
		delete s_copy;
}

void Scenario_SS_DS_VRPTW_CR::updateScenario(double currentTime) {
	if (sol_type == SS_VRPTW_CR) _ASSERT_(false, "not intended to be called with a SS-VRPTW-CR     currentTime=" << currentTime);
	_ASSERT_(false, "not implemented yet");
	UNUSED(currentTime);
}
void Scenario_SS_DS_VRPTW_CR::reSampleScenario(double currentTime) {
	if (sol_type == SS_VRPTW_CR) _ASSERT_(currentTime == -1, "not intended to be called with currentTime ≥ 0, in class Scenario_SS_DS_VRPTW_CR     currentTime=" << currentTime);

	currentTime = floor(currentTime);
	sampled_requests.resize(I.getNumberRegions());
	for (auto& vec : sampled_requests)
		vec.assign(I.getHorizon()+1, NULL);

	const set<VRP_vertex *>* v_set;
	switch(sol_type) {
		case SS_VRPTW_CR:
			v_set = & I.getCustomerVertices(); break;
		case DS_VRPTW:
			v_set = & I.getOnlineVertices(); break;
		default: _ASSERT_(false, "");
	}

	for (int ts = 0; ts <= I.getNumberTimeSlots(); ts++) {
		for (const VRP_vertex* pvertex : *v_set) {
			_ASSERT_(pvertex != nullptr, "");
			const VRP_request& r = pvertex->getRequest(ts);
			
			if (r.p <= 0) continue;
			if (currentTime >= 0 && I.revealedAtTime(r, currentTime)) continue;

			if (sol_type == SS_VRPTW_CR)
				ASSERT(r.revealTime >= 0, "argh");
			ASSERT(r.revealTime_min >= 0 && r.revealTime_max <= I.getHorizon(), "r.revealTime_min: " << r.revealTime_min << "    r.revealTime_max: " << r.revealTime_max);
			ASSERT((int)sampled_requests.size() >= pvertex->getRegion(), sampled_requests.size() << "<" << pvertex->getRegion());

			if(rand()%100 +1 <= r.p) {
				int rtime = max(r.revealTime_min, currentTime+1) + (rand() % (int)(r.revealTime_max - max(r.revealTime_min, currentTime+1) + 1));
				sampled_requests[pvertex->getRegion()][rtime] = &r;
				
			}
			// if(rand()%100 +1 <= r.p)
				// sampled_requests[pvertex->getRegion()][r.revealTime] = &r;
		}
	}


	sampled_requests_sequence.clear();

	for (int t = 0; t <= I.getHorizon(); t++) {
		vector<const VRP_request*> requests_shuffled;
		for (const VRP_vertex* pvertex : *v_set)
			if (sampled_requests[pvertex->getRegion()][t]) 
				requests_shuffled.push_back(sampled_requests[pvertex->getRegion()][t]);

		shuffle(begin(requests_shuffled), end(requests_shuffled), default_random_engine{});
		for (const VRP_request* r : requests_shuffled) {
			VRP_instance::RequestAttributes req_attr;
			req_attr.vertex = & r->getVertex();
			req_attr.request = r;
			req_attr.reveal_time = t;
			req_attr.time_slot = r->revealTS;
			sampled_requests_sequence.push_back(req_attr);
		}
			
	}

	sampled_time = currentTime;
}		



bool Scenario_SS_DS_VRPTW_CR::sampled(const VRP_request& r) const {
	return sampled_requests[r.getVertex().getRegion()][r.revealTime];
}

const VRP_request* Scenario_SS_DS_VRPTW_CR::sampled(int region, int time_unit) const {
	ASSERT(time_unit > 0 && time_unit <= I.getHorizon(), "argh");
	return sampled_requests[region][time_unit];
}

double Scenario_SS_DS_VRPTW_CR::evalSolution(const Solution_SS_VRPTW_CR& solution, RecourseStrategy strategy, bool debug, bool verbose, int scenarioHorizon) {
	switch (strategy) {
		case R_INFTY:
			return evalSolution_R_INFTY_CAPA(solution, false, debug, verbose, scenarioHorizon);
		case R_CAPA:
			return evalSolution_R_INFTY_CAPA(solution, true, debug, verbose, scenarioHorizon);
		case R_CAPA_PLUS:
			return evalSolution_R_CAPA_PLUS(solution, debug, verbose, scenarioHorizon);
		case R_BASIC_INFTY:
			return evalSolution_R_BASIC_INFTY_CAPA(solution, false, debug, verbose, scenarioHorizon);
		case R_BASIC_CAPA:
			return evalSolution_R_BASIC_INFTY_CAPA(solution, true, debug, verbose, scenarioHorizon);
		// case R_GSA:
		// 	return evalSolution_R_GSA(solution, verbose, scenarioHorizon);
		default: _ASSERT_(false, "");
	}
	return 0.0;
}

double Scenario_SS_DS_VRPTW_CR::evalSolution_R_BASIC_INFTY_CAPA(const Solution_SS_VRPTW_CR& solution, bool capacity, bool debug, bool verbose, int scenarioHorizon) const {
	(void) debug;
	ASSERT(scenarioHorizon == numeric_limits<int>::max(), "not implemented yet"); UNUSED(scenarioHorizon);
	const set<VRP_request> & requests_ordered = solution.getOrderedRequests();



	if (verbose) cout << "Scenario: evalSolution_R_BASIC_INFTY_CAPA() called" << endl;
	int nb_rejects = 0;

	// double current_time = 1;
	struct VehicleState {
		int id;
		double available_time = 1;
		int current_load = 0;
		const VRP_vertex *current_vertex = NULL;
	};
	vector<VehicleState> vehicles(solution.getNumberRoutes());

	// cout << "Using " << vehicles.size() << " vehicles";

	const VRP_vertex *depot = I.getVertexFromRegion(VRP_vertex::DEPOT, 0);
	int i = 1;
	for (VehicleState & veh : vehicles) {
		veh.id = i++;
		veh.current_vertex = depot;
	}

	int nb_reveals = 0;
	for (const VRP_request& r : requests_ordered) {
		if (sampled(r)) {
			nb_reveals++;
			for (VehicleState & veh : vehicles)
				veh.available_time = max(r.revealTime, veh.available_time);

			if (verbose) cout << endl;
			if (verbose) for (VehicleState & veh : vehicles) cout << "Vehicle " << veh.id << " at vertex " << veh.current_vertex->toString() << " : avail_time = " << veh.available_time << endl;
			if (verbose) cout << endl << "Request " << r.toString() << " appeared ! \t\t";
			// FIND THE CLOSEST VEHICLE TO THE REQUEST THAT IS ABLE TO SATISFY IT
			VehicleState * closest_veh = NULL;
			long min_travelTime = numeric_limits<int>::max();
			int min_current_load = 0;
			for (VehicleState & veh : vehicles) {
				const VRP_VehicleType& veh_type = solution.getVehicleTypeAtRoute(veh.id);

				int travel_time = veh_type.travelTime(*veh.current_vertex, r.getVertex());
				int return_to_depot_time = max(r.e, veh.available_time + travel_time) + veh_type.totalServiceTime(r) + veh_type.travelTime(r.getVertex(), *depot);
				ASSERT(veh_type.getCapacity() <= 1000, "argh");
				if (		   veh.available_time + travel_time <= r.l
							&& return_to_depot_time <= I.getHorizon() 
							&& (!capacity || veh.current_load + r.demand <= veh_type.getCapacity()) 
							&& (long)(travel_time * 10000 + veh.current_load) < (long)(min_travelTime * 10000 + min_current_load) ) {
							// && travel_time < min_travelTime) {
					min_travelTime = travel_time;
					min_current_load = veh.current_load;
					closest_veh = &veh;
				}
				if (verbose) cout << "VEH " << veh.id << ": avail=" << veh.available_time << "  travel=" << travel_time << "  return_depot=" << return_to_depot_time << "  ";  
			}
			if (verbose) cout << endl;
			if (closest_veh == NULL) {
				nb_rejects++;
				if (verbose) cout << "REJECTED" << endl;
			}
			else {
				const VRP_VehicleType& veh_type = solution.getVehicleTypeAtRoute(closest_veh->id);
				if (verbose) cout << "ACCEPTED -> vehicle " << closest_veh->id << " (at travel time " << veh_type.travelTime(*closest_veh->current_vertex, r.getVertex()) << ").";
				closest_veh->available_time = max(r.e, closest_veh->available_time + veh_type.travelTime(*closest_veh->current_vertex, r.getVertex())) + veh_type.totalServiceTime(r);
				closest_veh->current_load += r.demand;
				closest_veh->current_vertex = & r.getVertex();
				if (verbose) cout << "  Avail. time: " << closest_veh->available_time << endl;
			}
		}
	}

	for (VehicleState & veh : vehicles) {
		const VRP_VehicleType& veh_type = solution.getVehicleTypeAtRoute(veh.id);
		veh.available_time += veh_type.travelTime(*veh.current_vertex, *depot);
		// ASSERT(veh.available_time <= I.getHorizon(), "time=" << veh.available_time);
		ASSERT(!capacity || veh.current_load <= veh_type.getCapacity(), "load=" << veh.current_load);
	}

	if (verbose) cout << "# Rejections: " << nb_rejects << " / " << nb_reveals << endl;

	return nb_rejects;
}

double Scenario_SS_DS_VRPTW_CR::evalSolution_R_INFTY_CAPA(const Solution_SS_VRPTW_CR& solution, bool capacity, bool debug, bool verbose, int scenarioHorizon) const {
	ASSERT(scenarioHorizon == numeric_limits<int>::max(), "not implemented yet"); UNUSED(scenarioHorizon);
	const vector< vector< vector<VRP_request>>> & requests_ordered_per_waiting_locations = solution.getRequestAssignment();    // depends directly on the current first stage solution : [i][j][k] = r ==> request r is the k'th scheduled potential request of the j'th visited waiting vertex of route i
	ostringstream out; 
	out.setf(std::ios::fixed);
	out.precision(2);
	if (verbose || debug) out << "Scenario: evalSolution_R_INFTY_CAPA() called" << endl;
	int nb_rejects = 0;

	// ITERATING OVER ROUTES
	for (int route=1; route <= solution.getNumberRoutes(); route++) {
		const VRP_VehicleType& veh_type = solution.getVehicleTypeAtRoute(route);

		if (verbose || debug) out << "<-- ROUTE " << route << " -->" << endl;

		double current_time = 1;
		int current_load = 0;

		// ITERATING OVER PLANNED WAITING VERTICES
		Solution_SS_VRPTW_CR::SolutionElement current_w, prev_w;
		for (int wait_pos=1; wait_pos <= solution.getRouteSize(route)-1; wait_pos++) {
			solution.getSolutionElementAtPosition(&prev_w, route, wait_pos-1);
			solution.getSolutionElementAtPosition(&current_w, route, wait_pos);
			const VRP_vertex& waiting_vertex = solution.getVertexAtElement(current_w);
			ASSERT(waiting_vertex.getType() == VRP_vertex::WAITING, "");

			if (verbose || debug) out << "t=" << current_time;
			current_time += veh_type.travelTime(solution.getVertexAtElement(prev_w), waiting_vertex);
			if (verbose || debug) out << "\ttravel from " << solution.getVertexAtElement(prev_w).toString() << " to " << waiting_vertex.toString() << "(" << veh_type.travelTime(solution.getVertexAtElement(prev_w), waiting_vertex) << "); current_time=" << current_time << endl;
			
			ASSERT(current_time <= solution.getArrivalTimeAtElement(current_w), endl << solution.toString(true) << "vertex: " << waiting_vertex.toString() << "  current_time=" << current_time << "   arrivalTime: " << solution.getArrivalTimeAtElement(current_w) << endl << out.str() );
			current_time = max(current_time, solution.getArrivalTimeAtElement(current_w));
			

			// ITERATING OVER POTENTIAL REQUESTS
			for (const VRP_request& r : requests_ordered_per_waiting_locations[route][wait_pos]) {
				if (verbose || debug) out << "t=" << current_time << "\t\twaiting for " << r.toString() << " to reveal (" << r.revealTime << ");";
				
				// if (max(current_time, r.revealTime) <= solution.t_max(r, current_w) && (!capacity || current_load + r.demand <= I.getVehicleCapacity())) 		// if it is satisfiable from now and when revealed
					current_time = max(current_time, r.revealTime); 		// waiting until the requests reveals
					if (verbose || debug) out << "  current_time=" << current_time << endl;

				if (sampled(r)) {											// if the request reveals to be present
					if (verbose || debug) out << "t=" << current_time << "\t\t[sampled]" << endl;
					if (current_time <= solution.t_max(r, current_w) && (!capacity || current_load + r.demand <= veh_type.getCapacity())) {		// if it is satisfeasible
						if (verbose || debug) out << "t=" << current_time << "\t\t\tACCEPTED !  ";
						current_time = max(current_time, solution.t_min(r, current_w));		// wait until t_min and satisfy it
						if (verbose || debug) out << "\t\t\twaiting for t_min; current_time=" << current_time << endl;
						if (verbose || debug) out << "t=" << current_time;
						current_time += veh_type.travelTime(waiting_vertex, r.getVertex()) + veh_type.totalServiceTime(r) + veh_type.travelTime(r.getVertex(), waiting_vertex);
						current_load += r.demand;
						if (verbose || debug) out << "\t\t\tsatisfy " << r.toString() << "; current_time=" << current_time << endl;
					}
					else {														// if NOT satisfeasible
						if (verbose || debug) out << "t=" << current_time << "\t\t\tREJECTED !" << endl;
						nb_rejects ++;
					}
				} 
				// pool_volatile->hProbas_Q[r.id][current_time][current_load] += 1 / (double) pool_volatile->numberScenarios;
			}
		}
		ASSERT(current_time <= I.getHorizon(), "current_time=" << current_time << "   horizon=" << I.getHorizon() << endl << out.str());
		if (capacity) ASSERT(current_load <= veh_type.getCapacity(), "current_load=" << current_load << "   capacity=" << veh_type.getCapacity());
	}

	int n_unassigned_sampled = 0;
	for (auto& r : solution.getUnassignedRequests()) {
		if (sampled(r)) {
			if (verbose) out << "unassigned request " << r.toString(true) << " sampled" << endl;
			nb_rejects ++;
			n_unassigned_sampled ++;
		}
	}
	if (verbose) out << "Sampled unassigned requests: " << n_unassigned_sampled << "/" << solution.getUnassignedRequests().size() << endl;

	if (verbose) cout << out.str();

	return nb_rejects;
}


double Scenario_SS_DS_VRPTW_CR::evalSolution_R_CAPA_PLUS(const Solution_SS_VRPTW_CR& solution, bool debug, bool verbose, int scenarioHorizon) const {
	ostringstream out; 
	out.setf(std::ios::fixed);
	out.precision(2);
	ASSERT(scenarioHorizon == numeric_limits<int>::max(), "not implemented yet"); UNUSED(scenarioHorizon);
	const vector< vector< vector<VRP_request>>> & requests_ordered_per_waiting_locations = solution.getRequestAssignment();    // depends directly on the current first stage solution : [i][j][k] = r ==> request r is the k'th scheduled potential request of the j'th visited waiting vertex of route i

	if (verbose) cout << "Scenario: evalSolution_R_CAPA_PLUS() called" << endl;
	int nb_rejects = 0;
	int last_request_id = -1;
	UNUSED(last_request_id);

	// ITERATING OVER ROUTES
	for (int route=1; route <= solution.getNumberRoutes(); route++) {
		if (verbose) cout << "<-- ROUTE " << route << " -->" << endl;
		const VRP_VehicleType& veh_type = solution.getVehicleTypeAtRoute(route);

		double current_time = 1;
		int current_load = 0;


		Solution_SS_VRPTW_CR::SolutionElement current_w, next_w, depot;
		solution.getSolutionElementAtPosition(&depot, route, 0);
		const VRP_vertex* current_vertex = & solution.getVertexAtElement(depot);

		// ITERATING OVER PLANNED WAITING VERTICES
		for (int wait_pos=1; wait_pos <= solution.getRouteSize(route)-1; wait_pos++) {

			solution.getSolutionElementAtPosition(&current_w, route, wait_pos);
			solution.getSolutionElementAtPosition(&next_w, route, wait_pos+1);
			const VRP_vertex& waiting_vertex = solution.getVertexAtElement(current_w);

			if (verbose || debug) out << "t=" << current_time;
			current_time += veh_type.travelTime(*current_vertex, waiting_vertex);
			if (verbose || debug) out << "\ttravel from " << current_vertex->toString() << " to " << waiting_vertex.toString() << "(" << veh_type.travelTime(*current_vertex, waiting_vertex) << "); current_time=" << current_time << endl;
			
			ASSERT(current_time <= solution.getArrivalTimeAtElement(current_w), "current_time=" << current_time);
			current_time = max(current_time, solution.getArrivalTimeAtElement(current_w));
			
			current_vertex = & waiting_vertex;			

			// ITERATING OVER POTENTIAL REQUESTS
			auto& req_vector = requests_ordered_per_waiting_locations[route][wait_pos];
			for (auto it = req_vector.cbegin(); it != req_vector.cend(); it++) {
				const VRP_request& r = *it;
				
				if (verbose || debug) out << "t=" << current_time << "\t\twaiting for " << r.toString() << " to reveal (at t:" << r.revealTime << ")" << "\tcurrent_vertex=" << current_vertex->toString();
				// if (max(current_time, r.revealTime) <= solution.t_max_Rplus(r, *current_vertex, current_w) && current_load + r.demand <= I.getVehicleCapacity()) { // IF it is satisfiable from now and when sampled
					current_time = max(current_time, r.revealTime); 																								// waiting until the requests reveals
					if (verbose || debug) out << "  current_time=" << current_time << "\tcurrent_vertex=" << current_vertex->toString() << endl;
				// }

				if (sampled(r)) {		
					if (verbose || debug) out << "t=" << current_time << "\t\t[sampled]";																														// IF the request reveals to be present
					if (current_time <= solution.t_max_Rplus(r, *current_vertex, current_w) && current_load + r.demand <= veh_type.getCapacity()) {					// IF it is satisfeasible
						if (verbose || debug) out << "\tACCEPTED !  " << "\tcurrent_vertex=" << current_vertex->toString();	
						current_time = max(current_time, solution.t_min_Rplus(r, *current_vertex, current_w));															// wait until t_min_Rplus 
						if (verbose || debug) out << "\t\t\twaiting for t_min_Rplus; current_time=" << current_time << "\tcurrent_vertex=" << current_vertex->toString() << endl;						
						
						if (verbose || debug) out << "t=" << current_time;
						current_load += r.demand;			
						current_time += veh_type.travelTime(*current_vertex, r.getVertex()) + veh_type.totalServiceTime(r);																		// travel to it and satisfy it
						current_vertex = & r.getVertex();																													// it becomes the current location
						last_request_id = r.id;
						if (verbose || debug) out << "\t\t\tsatisfy " << r.toString() << "\tcurrent_time=" << current_time << "\tcurrent_vertex=" << current_vertex->toString() << endl;
					}			
					else {																																			// ELSE (if not satisfeasible)
						nb_rejects ++;	
						if (verbose || debug) out << "\tREJECTED !" << endl;																																// reject
					}	
				} 

				if ((it+1) != req_vector.cend()) {																											// IF not the last request of this waiting vertex
					if (current_time < (it+1)->revealTime) {	
						if (verbose || debug) out << "t=" << current_time;																									// IF next request not sampled
						current_time += veh_type.travelTime(*current_vertex, waiting_vertex);																					// travel back to waiting location
						if (verbose || debug) out << "\ttravel from " << current_vertex->toString() << " to " << waiting_vertex.toString() << "(" << veh_type.travelTime(*current_vertex, waiting_vertex) << "); current_time=" << current_time << endl;		
						current_vertex = & waiting_vertex;	
					}			
				}	

				// if (pool_volatile) {
				// 	pool_volatile->hProbas_Q[r.id][current_time][current_load] += 1 / (double) pool_volatile->numberScenarios;
				// 	if (current_vertex == & waiting_vertex)
				// 		pool_volatile->h_Qplus_w[r.id][current_time][current_load] += 1 / (double) pool_volatile->numberScenarios;
				// 	else
				// 		pool_volatile->h_Qplus[r.id][current_time][current_load][last_request_id] += 1 / (double) pool_volatile->numberScenarios;
				// }
			}
		}
		ASSERT(current_time <= I.getHorizon(), "current_time=" << current_time << "   horizon=" << I.getHorizon());
		ASSERT(current_load <= veh_type.getCapacity(), "current_load=" << current_load << "   capacity=" << veh_type.getCapacity());
	}

	int n_unassigned_sampled = 0;
	for (auto& r : solution.getUnassignedRequests()) {
		if (sampled(r)) {
			if (verbose || debug) out << "unassigned request " << r.toString(true) << " sampled" << endl;
			nb_rejects ++;
			n_unassigned_sampled ++;
		}
	}
	if (verbose || debug) out << "Sampled unassigned requests: " << n_unassigned_sampled << "/" << solution.getUnassignedRequests().size() << endl;

	if (verbose) cout << out.str();

	return nb_rejects;
}






double Scenario_SS_DS_VRPTW_CR::evalSolution_R_GSA(const Solution_DS_VRPTW& solution, bool verbose, int scenarioHorizon) {
	// _ASSERT_(solution.getCurrentTime() <= this->sampled_time + 1, "");
	if (s_copy == nullptr) {
		s_copy = new Solution_DS_VRPTW(solution);
		// cout << endl << "new called" << endl;
	}
	else
		*s_copy = solution;

	ASSERT(scenarioHorizon == numeric_limits<int>::max(), "not implemented yet"); UNUSED(scenarioHorizon);
	
	if (verbose) cout << endl << "Scenario: evalSolution_R_GSA() called (current time: " << solution.getCurrentTime() <<")" << endl;
	int nb_rejects = 0;

	for (const VRP_instance::RequestAttributes& req_attr : sampled_requests_sequence) {
		if (s_copy->getCurrentTime() < req_attr.reveal_time) 
			s_copy->setCurrentTime(req_attr.reveal_time);

		if (verbose) cout << "Time " << req_attr.reveal_time << ": ";
		bool success = s_copy->tryInsertOnlineRequest(*req_attr.request, false);
		if (! success) nb_rejects++;
		if (verbose) cout << outputMisc::boolColorExpr(success) << req_attr.request->toString() << outputMisc::resetColor() << "  " << endl;
	}


	if (verbose) cout << "Total: " << nb_rejects << " rejects." << endl;


	return nb_rejects;
}


std::string& Scenario_SS_DS_VRPTW_CR::toString() const {
	ostringstream out;
	out.setf(std::ios::fixed);
	out.precision(2);

	out << "Scenario: " << endl;
	// for (int t = 0; t <= I.getHorizon(); t++)
	// 	for (int region = 1; region < I.getNumberRegions(); region++)
	// 		if (sampled_requests[region][t]) 
	// 			out << "\t" << "Time " << setw(4) << t << " (time slot " << setw(3) << sampled_requests[region][t]->revealTS << ") ~> Request at vertex " << sampled_requests[region][t]->getVertex().toString() << "  req. infos: " << sampled_requests[region][t]->toString(true) << "  (appeared flag =" << sampled_requests[region][t]->hasAppeared() << ")" << endl;
	for (const VRP_instance::RequestAttributes& req_attr : sampled_requests_sequence)  {
		out << "\t" << "Time " << setw(4) << req_attr.reveal_time << " (time slot " << setw(3) << req_attr.time_slot << ") ~> Request at vertex " << req_attr.vertex->toString() << "  req. infos: " << req_attr.request->toString(true) << "  (appeared flag =" << req_attr.request->hasAppeared() << ")" << endl;
	}

	static string str = ""; str = out.str(); return (str);
}

std::string& Scenario_SS_DS_VRPTW_CR::toString_perRegion() const {
	ostringstream out;
	out.setf(std::ios::fixed);
	out.precision(2);

	out << "Scenario: " << endl;
	for (int region = 1; region < I.getNumberRegions(); region++)
		for (int t = 0; t <= I.getHorizon(); t++)
			if (sampled_requests[region][t]) 
				out << "\tRequest at vertex " << sampled_requests[region][t]->getVertex().toString() << " sampled for time " << setw(3) << t << " (time slot " << setw(2) << sampled_requests[region][t]->revealTS << ")  req. infos: " << sampled_requests[region][t]->toString(true) << "  (appeared flag =" << sampled_requests[region][t]->hasAppeared() << ")" << endl;

	static string str = ""; str = out.str(); return (str);
}