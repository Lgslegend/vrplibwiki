#include "../VRP_instance.h"
#include "../VRP_solution.h"
#include "../VRP_scenario.h"
#include "../NM.h"
#include "../LS_Program.h"
#include "../tools.h"
#include <queue>
#include <cmath>
#include <limits>

using namespace std;

bool VRP_request::operator<(const VRP_request& other) const {
	return id_instance_file < other.id_instance_file;
}

int main(int argc, char **argv) {
	if (argc < 2) {
		cout << "Usage: test_VRPTW instance_file" << endl;
		return 1;
	}
	const char* instance_file = argv[1];

	srand(time(NULL));

	// INSTANCE FILE
	VRP_instance& I = VRP_instance_file::readInstanceFile_CordeauLaporte_VRPTW_old(instance_file);
	cout << I.toString() << endl << I.toStringCoords(true) << endl;
	for (const VRP_VehicleType veh_type : I.getVehicleTypes())
		cout << veh_type.toStringDistances() << endl;

	// SOLUTION 
	Solution_VRPTW s(I);
	s.addRoutes(I._getNumberVehicles());

	// NEIGHBORHOOD MANAGER
	NeighborhoodManager<Solution_VRPTW> nm(s, VRPTW);


	// MAIN
	s.generateInitialSolution();
	cout << "Initial solution:" << endl << s.toString(false) << s.toString(true) << endl;
	cout << "Cost initial solution: " << s.getCost()  << "   violations: " <<  s.getWeightViolations()<< endl;
	
	cout << "LS ..." << endl;
	
	// LS_Program_Basic<Solution_VRPTW,NeighborhoodManager<Solution_VRPTW>> lsprog;
	// lsprog.run(s, nm, 10000000, 5, 3000, true);			// args: (Solution &solution, NM &nm, int max_iter, int diversification = 10, int max_distance = 2500, bool verbose = false)
	LS_Program_SimulatedAnnealing<Solution_VRPTW,NeighborhoodManager<Solution_VRPTW>> lsprog;
	lsprog.setTimeOut(2);	
	lsprog.run(s, nm, numeric_limits<int>::max(), 5.0, 0.995, true);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)

	cout << s.toString(false) << s.toString(true) << endl;
	cout << "Cost: " << s.getCost()  << "   violations: " <<  s.getWeightViolations()<< endl;

}



