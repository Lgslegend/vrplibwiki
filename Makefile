HOSTNAME := $(shell hostname | cut -b -4)


ifeq ($(HOSTNAME),mac-)
	export MAKEFLAGS = "-j $(getconf _NPROCESSORS_ONLN)"
	CC		= g++
	CFLAGS	= -g -O3 -Wall -W -std=c++11 #-DASSERTS
else
	ifeq ($(HOSTNAME),cca0)	# CC-IN2P3 cluster
		GCCDIR = /afs/in2p3.fr/home/m/msaintgu/private/gcc-dev
	endif
	ifeq ($(HOSTNAME),clau)
		export MAKEFLAGS = "-j 4"
		GCCDIR = /etinfo/users2/stguillain/gcc-dev
	endif
	ifeq ($(HOSTNAME),drag)	# Dragon1 cluster
		export MAKEFLAGS = "-j $(getconf _NPROCESSORS_ONLN)"
		GCCDIR = /home/ucl/ingi/stguilla/gcc/gcc-dev-7.1.0
	endif
	ifeq ($(HOSTNAME),hmem) # HMEM cluster
		export MAKEFLAGS = "-j $(getconf _NPROCESSORS_ONLN)"	
		GCCDIR = /home/ucl/ingi/stguilla/gcc/gcc-dev-7.1.0
	endif
	ifeq ($(HOSTNAME),node)	# VEGA cluster
		export MAKEFLAGS = "-j $(getconf _NPROCESSORS_ONLN)"
		GCCDIR = /home/ucl/ingi/stguilla/gcc/gcc-dev-7.1.0
	endif
	ifeq ($(HOSTNAME),lema)	# LEMAITRE2 cluster
		export MAKEFLAGS = "-j $(getconf _NPROCESSORS_ONLN)"
		GCCDIR = /home/ucl/ingi/stguilla/gcc/gcc-dev-7.1.0
	endif

	CC		= $(GCCDIR)/bin/g++
	export LD_LIBRARY_PATH	= $(GCCDIR)/gcc-dev/lib:$(GCCDIR)/lib64
	export GCC_EXEC_PREFIX	= $(GCCDIR)/lib/gcc/
	# CFLAGS	= -pg -g -O3 -Wall -W -L/etinfo/users2/stguillain/gcc-dev/lib64 -I/etinfo/users2/stguillain/gcc-dev/include -B/etinfo/users2/stguillain/gcc-dev/lib/gcc/ -std=c++11	# -pg option for GCC is used only for profiler ! -g is for debugging !
	CFLAGS	= -O3 -Wall -W -L$(GCCDIR)/lib64 -I$(GCCDIR)/include -B$(GCCDIR)/lib/gcc/ -std=c++11
endif
LIBPATH = ../lib

SRC		= 	VRP_instance.cpp		\
			VRP_routes.cpp			\
			VRP_solution.cpp		\
			VRP_scenario.cpp		\
			readInstanceFiles.cpp

INC		= 	VRP_instance.h				\
			VRP_routes.h				\
			VRP_solution.h				\
			VRP_scenario.h				\
			NM.h						\
			LS_Program.h



OBJ		= $(SRC:.cpp=.o)

all: $(OBJ) $(SRC) $(INC)

.cpp.o: 
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm $(PROGS) *.o 


.PHONY: clean