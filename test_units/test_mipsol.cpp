#include "../VRP_instance.h"
#include "../VRP_solution.h"
#include "../VRP_scenario.h"
#include "../NM.h"
#include "../LS_Program.h"
#include "../tools.h"
#include <queue>
#include <cmath>
#include <limits>

using namespace std;

bool VRP_request::operator<(const VRP_request& other) const {
	if (operator==(other)) return false;

	if (revealTime < other.revealTime)							// reveal time first
		return true;
	if (revealTime == other.revealTime) {
		if (l < other.l)										// end of time window second)
			return true;
		if (l == other.l) {
			ASSERT(vertex->region != other.vertex->region, "no tie breaking !");
			return vertex->region < other.vertex->region;		// region number to break ties
		}
	}

	return false;
}

int main(int argc, char **argv) {
	(void) argc;
	(void) argv;

	srand(time(NULL));

	if (argc < 3) {
		cout << "Usage: ssvrptwcr_cplex travel_times_file instance_file" << endl;
		return 1;
	}
	const char * travel_times_file = argv[1];
	const char * instance_file = argv[2];
	
	// VRP_instance &I = VRP_instance_file::readInstanceFile_SSVRPTW_CR("TU_instance_SSVRPTWCR_1.txt", 10.0);
	// VRP_instance &I = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs("../../benchmarks/ss-vrptw-cr/realdata_optimod_lyon/instances/travel_times.txt", "../../benchmarks/ss-vrptw-cr/realdata_optimod_lyon/instances/instance_1.txt");
	int capacity = 0;
	VRP_instance &I = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, capacity, 1.0);


	int NVertices = I.getNumberVertices(VRP_vertex::WAITING);
	int NVeh = 2;
	int H = I.getHorizon();

	cout << I.toString() << endl;
	// RecourseStrategy strategy = R_INFTY;	
	// RecourseStrategy strategy = R_CAPA;	
	// RecourseStrategy strategy = R_CAPA_PLUS;	

	Solution_SS_VRPTW_CR s(I, R_CAPA);
	s.addRoutes(NVeh);
	s.generateInitialSolution(true);

	double ***vars_x;
	vars_x = new double**[NVertices+1];
	for (int i = 0; i < NVertices+1; i++) {
		vars_x[i] = new double*[NVertices+1];
		for (int j = 0; j < NVertices+1; j++) {
			vars_x[i][j] = new double[NVeh+1];
			for(int p = 0; p < NVeh+1; p++)
				vars_x[i][j][p] = 0.0;
		}
	}
	double ***vars_tau;
	vars_tau = new double**[NVertices+1];
	for (int i = 0; i < NVertices+1; i++) {
		vars_tau[i] = new double*[H+1];
		for (int l = 0; l < H+1; l++) {
			vars_tau[i][l] = new double[NVeh+1];
			for(int p = 0; p < NVeh+1; p++)
				vars_tau[i][l][p] = 0.0;
		}
	}

	vars_x[0][0][1] = 0;	vars_x[0][1][1] = 0;	vars_x[0][2][1] = 0;	vars_x[0][3][1] = 0;	vars_x[0][4][1] = 1;	vars_x[0][5][1] = 0;	vars_x[0][6][1] = 0;	vars_x[0][7][1] = 0;	vars_x[0][8][1] = 0;	vars_x[0][9][1] = 0;	vars_x[0][10][1] = 0;
	vars_x[1][0][1] = 0;	vars_x[1][1][1] = 0;	vars_x[1][2][1] = 0;	vars_x[1][3][1] = 0;	vars_x[1][4][1] = 0;	vars_x[1][5][1] = 0;	vars_x[1][6][1] = 0;	vars_x[1][7][1] = 0;	vars_x[1][8][1] = 0;	vars_x[1][9][1] = 0;	vars_x[1][10][1] = 0;
	vars_x[2][0][1] = 1;	vars_x[2][1][1] = 0;	vars_x[2][2][1] = 0;	vars_x[2][3][1] = 0;	vars_x[2][4][1] = 0;	vars_x[2][5][1] = 0;	vars_x[2][6][1] = 0;	vars_x[2][7][1] = 0;	vars_x[2][8][1] = 0;	vars_x[2][9][1] = 0;	vars_x[2][10][1] = 0;
	vars_x[3][0][1] = 0;	vars_x[3][1][1] = 0;	vars_x[3][2][1] = 0;	vars_x[3][3][1] = 0;	vars_x[3][4][1] = 0;	vars_x[3][5][1] = 0;	vars_x[3][6][1] = 0;	vars_x[3][7][1] = 0;	vars_x[3][8][1] = 0;	vars_x[3][9][1] = 0;	vars_x[3][10][1] = 0;
	vars_x[4][0][1] = 0;	vars_x[4][1][1] = 0;	vars_x[4][2][1] = 0;	vars_x[4][3][1] = 0;	vars_x[4][4][1] = 0;	vars_x[4][5][1] = 0;	vars_x[4][6][1] = 1;	vars_x[4][7][1] = 0;	vars_x[4][8][1] = 0;	vars_x[4][9][1] = 0;	vars_x[4][10][1] = 0;
	vars_x[5][0][1] = 0;	vars_x[5][1][1] = 0;	vars_x[5][2][1] = 0;	vars_x[5][3][1] = 0;	vars_x[5][4][1] = 0;	vars_x[5][5][1] = 0;	vars_x[5][6][1] = 0;	vars_x[5][7][1] = 0;	vars_x[5][8][1] = 1;	vars_x[5][9][1] = 0;	vars_x[5][10][1] = 0;
	vars_x[6][0][1] = 0;	vars_x[6][1][1] = 0;	vars_x[6][2][1] = 0;	vars_x[6][3][1] = 0;	vars_x[6][4][1] = 0;	vars_x[6][5][1] = 0;	vars_x[6][6][1] = 0;	vars_x[6][7][1] = 1;	vars_x[6][8][1] = 0;	vars_x[6][9][1] = 0;	vars_x[6][10][1] = 0;
	vars_x[7][0][1] = 0;	vars_x[7][1][1] = 0;	vars_x[7][2][1] = 0;	vars_x[7][3][1] = 0;	vars_x[7][4][1] = 0;	vars_x[7][5][1] = 1;	vars_x[7][6][1] = 0;	vars_x[7][7][1] = 0;	vars_x[7][8][1] = 0;	vars_x[7][9][1] = 0;	vars_x[7][10][1] = 0;
	vars_x[8][0][1] = 0;	vars_x[8][1][1] = 0;	vars_x[8][2][1] = 1;	vars_x[8][3][1] = 0;	vars_x[8][4][1] = 0;	vars_x[8][5][1] = 0;	vars_x[8][6][1] = 0;	vars_x[8][7][1] = 0;	vars_x[8][8][1] = 0;	vars_x[8][9][1] = 0;	vars_x[8][10][1] = 0;
	vars_x[9][0][1] = 0;	vars_x[9][1][1] = 0;	vars_x[9][2][1] = 0;	vars_x[9][3][1] = 0;	vars_x[9][4][1] = 0;	vars_x[9][5][1] = 0;	vars_x[9][6][1] = 0;	vars_x[9][7][1] = 0;	vars_x[9][8][1] = 0;	vars_x[9][9][1] = 0;	vars_x[9][10][1] = 0;
	vars_x[10][0][1] = 0;	vars_x[10][1][1] = 0;	vars_x[10][2][1] = 0;	vars_x[10][3][1] = 0;	vars_x[10][4][1] = 0;	vars_x[10][5][1] = 0;	vars_x[10][6][1] = 0;	vars_x[10][7][1] = 0;	vars_x[10][8][1] = 0;	vars_x[10][9][1] = 0;	vars_x[10][10][1] = 0;
////
	vars_x[0][0][2] = 0;	vars_x[0][1][2] = 0;	vars_x[0][2][2] = 0;	vars_x[0][3][2] = 0;	vars_x[0][4][2] = 0;	vars_x[0][5][2] = 0;	vars_x[0][6][2] = 0;	vars_x[0][7][2] = 0;	vars_x[0][8][2] = 0;	vars_x[0][9][2] = 0;	vars_x[0][10][2] = 1;
	vars_x[1][0][2] = 1;	vars_x[1][1][2] = 0;	vars_x[1][2][2] = 0;	vars_x[1][3][2] = 0;	vars_x[1][4][2] = 0;	vars_x[1][5][2] = 0;	vars_x[1][6][2] = 0;	vars_x[1][7][2] = 0;	vars_x[1][8][2] = 0;	vars_x[1][9][2] = 0;	vars_x[1][10][2] = 0;
	vars_x[2][0][2] = 0;	vars_x[2][1][2] = 0;	vars_x[2][2][2] = 0;	vars_x[2][3][2] = 0;	vars_x[2][4][2] = 0;	vars_x[2][5][2] = 0;	vars_x[2][6][2] = 0;	vars_x[2][7][2] = 0;	vars_x[2][8][2] = 0;	vars_x[2][9][2] = 0;	vars_x[2][10][2] = 0;
	vars_x[3][0][2] = 0;	vars_x[3][1][2] = 1;	vars_x[3][2][2] = 0;	vars_x[3][3][2] = 0;	vars_x[3][4][2] = 0;	vars_x[3][5][2] = 0;	vars_x[3][6][2] = 0;	vars_x[3][7][2] = 0;	vars_x[3][8][2] = 0;	vars_x[3][9][2] = 0;	vars_x[3][10][2] = 0;
	vars_x[4][0][2] = 0;	vars_x[4][1][2] = 0;	vars_x[4][2][2] = 0;	vars_x[4][3][2] = 0;	vars_x[4][4][2] = 0;	vars_x[4][5][2] = 0;	vars_x[4][6][2] = 0;	vars_x[4][7][2] = 0;	vars_x[4][8][2] = 0;	vars_x[4][9][2] = 0;	vars_x[4][10][2] = 0;
	vars_x[5][0][2] = 0;	vars_x[5][1][2] = 0;	vars_x[5][2][2] = 0;	vars_x[5][3][2] = 0;	vars_x[5][4][2] = 0;	vars_x[5][5][2] = 0;	vars_x[5][6][2] = 0;	vars_x[5][7][2] = 0;	vars_x[5][8][2] = 0;	vars_x[5][9][2] = 0;	vars_x[5][10][2] = 0;
	vars_x[6][0][2] = 0;	vars_x[6][1][2] = 0;	vars_x[6][2][2] = 0;	vars_x[6][3][2] = 0;	vars_x[6][4][2] = 0;	vars_x[6][5][2] = 0;	vars_x[6][6][2] = 0;	vars_x[6][7][2] = 0;	vars_x[6][8][2] = 0;	vars_x[6][9][2] = 0;	vars_x[6][10][2] = 0;
	vars_x[7][0][2] = 0;	vars_x[7][1][2] = 0;	vars_x[7][2][2] = 0;	vars_x[7][3][2] = 0;	vars_x[7][4][2] = 0;	vars_x[7][5][2] = 0;	vars_x[7][6][2] = 0;	vars_x[7][7][2] = 0;	vars_x[7][8][2] = 0;	vars_x[7][9][2] = 0;	vars_x[7][10][2] = 0;
	vars_x[8][0][2] = 0;	vars_x[8][1][2] = 0;	vars_x[8][2][2] = 0;	vars_x[8][3][2] = 0;	vars_x[8][4][2] = 0;	vars_x[8][5][2] = 0;	vars_x[8][6][2] = 0;	vars_x[8][7][2] = 0;	vars_x[8][8][2] = 0;	vars_x[8][9][2] = 0;	vars_x[8][10][2] = 0;
	vars_x[9][0][2] = 0;	vars_x[9][1][2] = 0;	vars_x[9][2][2] = 0;	vars_x[9][3][2] = 0;	vars_x[9][4][2] = 0;	vars_x[9][5][2] = 0;	vars_x[9][6][2] = 0;	vars_x[9][7][2] = 0;	vars_x[9][8][2] = 0;	vars_x[9][9][2] = 0;	vars_x[9][10][2] = 0;
	vars_x[10][0][2] = 0;	vars_x[10][1][2] = 0;	vars_x[10][2][2] = 0;	vars_x[10][3][2] = 1;	vars_x[10][4][2] = 0;	vars_x[10][5][2] = 0;	vars_x[10][6][2] = 0;	vars_x[10][7][2] = 0;	vars_x[10][8][2] = 0;	vars_x[10][9][2] = 0;	vars_x[10][10][2] = 0;


	vars_tau[1][420][1] = 0;
	vars_tau[2][60][1] = 1;
	vars_tau[3][120][1] = 0;
	vars_tau[4][60][1] = 1;
	vars_tau[5][60][1] = 1;
	vars_tau[6][60][1] = 1;
	vars_tau[7][60][1] = 1;
	vars_tau[8][60][1] = 1;
	vars_tau[9][60][1] = 0;
	vars_tau[10][60][1] = 0;

	vars_tau[1][180][2] = 1;
	vars_tau[2][180][2] = 0;
	vars_tau[3][60][2] = 1;
	vars_tau[4][60][2] = 0;
	vars_tau[5][60][2] = 0;
	vars_tau[6][420][2] = 0;
	vars_tau[7][420][2] = 0;
	vars_tau[8][120][2] = 0;
	vars_tau[9][60][2] = 0;
	vars_tau[10][120][2] = 1;





	for (int i = 0; i < NVertices+1; i++) 
		for (int j = 0; j < NVertices+1; j++)
			for (int p = 1; p < NVeh+1; p++)
				s.mipsol_setX(i, j, p, vars_x[i][j][p]);
	s.update_routes_from_mipsol();

	for (int i = 0; i < NVertices+1; i++) 
		for (int l = 0; l < H+1; l++)
			for (int p=1; p<NVeh+1; p++)
				s.mipsol_setTAU(i,l,p, vars_tau[i][l][p]);

	s.update_waitingTimes_from_mipsol();


	// cout << s.toString() ;
	// cout << s.toString(true) << endl;

	// cout << "ARCS:       \t";
	// for (auto& arc : s.mipsol_getArcs())
	// 	cout << "(" << arc.i << "," << arc.j << "," << arc.p << ") ";
	// cout << endl;
	// cout << "WAIT. TIMES:\t";
	// for (auto& wt : s.mipsol_getWaitTimes())
	// 	cout << "(" << wt.i << "," << wt.l << "," << wt.p << ") ";
	// cout << endl;


	// const vector<vector<Solution_SS_VRPTW_CR::MIPSol_Arc>> & subtours = s.mipsol_getSubtours();
	// if (subtours.size() > 0) {
	// 	cout << "SUBTOURS:  ";
	// 	for (auto& subtour : subtours) {
	// 		for (auto& arc : subtour)
	// 			cout << "(" << arc.i << "," << arc.j << ") ";
	// 		cout << " - ";
	// 	}
	// 	cout << endl;
	// }

	// VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR> scenarioPool(I, 100000, SS_VRPTW_CR);
	// double experimental = scenarioPool.computeExperimentalExpectedCost(s, R_CAPA, true, false);
	// cout << "R_CAPA Exp. cost: " << s.getCost() << "   Experimental expected cost: " << experimental << endl;

	cout << s.toString() << endl << endl;

	cout << "R_CAPA Exp. cost: \t" << s.getCost() << endl;

	s.setRecourseStrategy(R_CAPA_PLUS);

	cout << "R_CAPA+ Exp. cost: \t" << s.getCost() << endl;

	// VRP_ScenarioPool_Volatile<Scenario_SS_VRPTW_CR, Solution_SS_VRPTW_CR> scenarioPool_2(I, 1);
	// scenarioPool_2.computeExperimentalExpectedCost(s, R_CAPA_PLUS, false, true);

	// // s.toString_h_v(route, request_id, n)
	// cout << s.toString_hProbas() << endl;

	// VRP_instance& I480 = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, capacity, 1.0);
	// cout << "Solution under 480 horizon: " << endl;
	// Solution_SS_VRPTW_CR s480(I480, strategy);
	// s480.addRoutes(1);
	// s480.generateInitialSolution(true);
	// s480.copyFromSol_scaleWaitingTimes(s);
	// cout << s480.toString(false) << s480.getCost() <<  endl;
}


