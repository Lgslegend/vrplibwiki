/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/


/* NeighborhoodManager ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	An instance of NeighborhoodManager class represents a toolbox for creating neighboring solutions 
	from a given current VRP solution.

	The class holds a solution, represented as a template S object, and provides a number of public 
	methods, amoungst them shakeSolution(), intensify() and diversify(). 
	- shakeSolution() applies a neighborhood operator in order create a new solution from the current one;
	- intensify() informs the class that it should intensify the search, by focusing on neighorhood 
		solutions that are close to the current solution;
	- diversify() does the opposite of intensify().

	NeighborhoodManager reproduces the approach of a Variable Neighborhood Search (VNS), by varying, at
	call of shakeSolution(), both the neighborhood operator used and the scale size applied to it (when
	possible).

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/


#ifndef NM_H
#define	NM_H

// #include "VRP_solution.h"

using namespace std;

#include <set>
#include <limits>

template <class S>
class NeighborhoodManager {
	typedef typename S::SolutionElement SolutionElement;
protected:
	enum Constants {
		MAX_OPERATOR_SIZE 			= 6,	// maximum size of each operator (when applicable)
		
		/* Operator numbering */
		RELOCATE_RANDOM 			= 1, 	
		TWO_OPT_INVERSION 			= 2,	
		TWO_EXCHANGE 				= 3,	
		CROSS_EXCHANGE 				= 4,	

		RELOCATE_WORST_TO_BEST 		= 10,
		RELOCATE_BEST_IMPROVEMENT 	= 11,

		INSERT_WAITING_VERTEX 		= 100,
		REMOVE_WAITING_VERTEX 		= 101,
		ADD_WAITING_TIME 			= 102,
		REMOVE_WAITING_TIME 		= 103,
		TRANSFER_WAITING_TIME 		= 104
	};
	const int SSVRP_SEQUENCE[9] 	= {1, 2, 3, 4, 102, 104, 101, 100, 103};// 	"SS_VRPTW_CR"
	const int DSVRP_GSA_SEQUENCE[6] = {100, 1, 2, 3, 4, 101};				// 	"DS_VRPTW_GSA"
	const int VRP_SEQUENCE_NODIFF[4]= {1, 2, 3, 4};							//	"DS_VRPTW"
	const int VRP_SEQUENCE[4] 		= {1, 2, 3, 4};							//	"TSP, MTSP, VRP*"
	
	long int it = 0;
	int current_sequence_idx;			// current operator in the sequence
	int current_operator_size;			// current operator size
	int success[1000];					// records the number of improvements obtained using each operator
	int last_operator_number_called;	// records the number (ex: 103) of the last operator called
	SolutionElement last_route_1, last_route_2;		// used to remember the involved elements of the last operation (useful to restore the previous solution !)
	set<SolutionElement> modifiedRoutes;		// record all the routes that have been modified since the last call to restorePreviousSolution()

	S& 	solution;					// reference to the object (SolutionVRP, SolutionVRPTW, SolutionSSVRPTW, ...) representing the current solution
	VRP_type sol_vrp_type;
	int horizon;
	int min_increment, max_increment;	// the min and max increment/decrement we apply to modify waiting time; values are chosen at random in [min_increment, max_increment]
	// double waiting_time_multiple = 1.0;
	// double (*costPerArc) (VRP_vertex &, VRP_vertex &);	// pointer to a function that returns the cost associated to one arc (v1,v2) in the solution, with v1 and v2 instances of type VRP_vertex
	// double (*nbViolations) (VRP_vertex &);				// pointer to a function that returns the number of violations associated to one vertex in the solution

	/* classical context */
	bool relocate_random(bool verbose=false);					
	bool relocate_worst_to_best(bool verbose=false);					
	bool relocate_best_improvement(bool verbose=false);
	bool two_exchange(bool verbose=false);
	bool two_opt_inversion(int max_size, bool verbose=false);
	bool cross_exchange(int max_size, bool verbose=false);

	/* dynamic context */
	bool insert_waiting_vertex(bool verbose=false);
	bool remove_waiting_vertex(bool verbose=false);
	bool add_waiting_time(bool verbose=false);
	bool transfer_waiting_time(bool verbose=false);
	bool remove_waiting_time(bool verbose=false);

	bool applyOperator(int operator_, bool verbose);
public:
	NeighborhoodManager(S& solution, VRP_type vrp_type, int horizon = numeric_limits<int>::max(), int min_increment = 1, int max_increment = numeric_limits<int>::max());
	bool shakeSolution(bool verbose=false);				// Moves to a neighboring solution, by using one of the neighborhood operators; returns false iff nothing happened !
	void intensify();
	void diversify();
	void printNeighborhoodScores() const;
	void neighborAccepted();
	void restorePreviousSolution(S& from_solution, bool verbose=false);
};


template <class S>
class NeighborhoodManager_Adaptative : public NeighborhoodManager<S> { 
protected:
	double 	score[NeighborhoodManager<S>::K + NeighborhoodManager<S>::MAX_K_SIZE];
	int 	segSuccess[NeighborhoodManager<S>::K + NeighborhoodManager<S>::MAX_K_SIZE];
	int 	attempt[NeighborhoodManager<S>::K + NeighborhoodManager<S>::MAX_K_SIZE];
	int 	segAttempt[NeighborhoodManager<S>::K + NeighborhoodManager<S>::MAX_K_SIZE];
	int 	segment;
	int 	last_size;
public:
	NeighborhoodManager_Adaptative(S& solution);
	int shakeSolution(bool verbose=false);
	void neighborAccepted();						 
	void printNeighborhoodScores() const;
};



#include "NM.cpp"


#endif


