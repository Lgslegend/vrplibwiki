/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/




/* VRP_scenario +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	This class is used to represent a VRP scenario / scenario pool, in a dynamic context.

	The constructors usually take an object of type VRP_instance, describing the current problem.
	Concerning VRP_ScenarioPool, one also precise the desired number of scenari in the pool. 

	The goal of a VRP_ScenarioPool object is to compute expected cost, based on random event (which 
	are stochastically described in the instance). 
	In order to compute an expected cost, one provides a solution object of template type Solution, 
	and the VRP_ScenarioPool then assess the operational actions described in that solution to each
	and every scenario of its pool, in order to compute the expectation. 
	How a solution is confronted to a scenario is directly defined in the evalSolution() method of
	the corresponding Scenario class.

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/

#ifndef VRP_SCENARIO_H
#define	VRP_SCENARIO_H

#include <iostream>
#include <vector>
#include <set>
#include <cmath>
#include <limits>
#include "VRP_instance.h"
#include "VRP_solution.h"
// #include "types.h"
#include "tools.h"







template <class Scenario, class Solution> 
class VRP_ScenarioPool {
protected:
	const VRP_instance& I;
	vector<Scenario> scenarioPool;	
	int numberScenarios;
	VRP_type sol_type;
private:
	// vector<vector<vector<double>>> hProbas_Q;		// for debugging

public:

	VRP_ScenarioPool(const VRP_instance& instance, int poolSize, VRP_type vrp_type, int currentTime = -1) : I(instance) {
		ASSERT(poolSize > 0, "argh");
		ASSERT(vrp_type != VRPTYPE_NO_SET, "");
		sol_type = vrp_type;
		numberScenarios = poolSize;
		for (int i=0; i<poolSize; i++)
			scenarioPool.push_back(Scenario(instance, sol_type, currentTime));
	}

	const vector<Scenario> & getScenarios() const { return scenarioPool; }

	void updatePool(double currentTime) {
		for (auto& s : scenarioPool)
			s.updateScenario(currentTime);
	}
	int reSamplePool(double currentTime, float proportion = 1.0) {
		
		if(proportion <= 0.0)
			return 0;

		if(proportion == 1.0) {
			for (auto& s : scenarioPool)
				s.reSampleScenario(currentTime);
			return scenarioPool.size();
		}

		double nReSample = (double) scenarioPool.size() * proportion;
		int nReS = floor(nReSample) + (rand() % 100000 + 1 <= (nReSample-floor(nReSample))*100000); // decide probabilistically for the remaining fraction
		if(nReS > 0) {
			int fromPos = rand() % (scenarioPool.size()-nReS+1);
			for(int i=fromPos; i<fromPos+nReS; i++) {
				scenarioPool[i].reSampleScenario(currentTime);
			}		
		}
		return nReS;
	}

	double computeExperimentalExpectedCost(const Solution& solution, RecourseStrategy strategy, bool debug=false, bool verbose = false, int scenarioHorizon = numeric_limits<int>::max()) {
		ASSERT(scenarioHorizon == numeric_limits<int>::max(), "not implemented yet");

		double eval = 0.0;
		for (auto& s : scenarioPool)
			eval += s.evalSolution(solution, strategy, debug, verbose, scenarioHorizon);

		return eval / scenarioPool.size();
	}

	std::string& toString() const {
		ostringstream out;
		for (auto& s : scenarioPool)
			out << s.toString() << endl << endl;
		static string str = ""; str = out.str(); return (str);
	}
	std::string& toString_perRegion() const {
		ostringstream out;
		for (auto& s : scenarioPool)
			out << s.toString_perRegion() << endl << endl;
		static string str = ""; str = out.str(); return (str);
	}


	double evalSolution_GSA(const Solution& solution, bool verbose = false, int scenarioHorizon = numeric_limits<int>::max()) {
		ASSERT(scenarioHorizon == numeric_limits<int>::max(), "not implemented yet");

		double eval = 0.0;
		for (auto& s : scenarioPool)
			eval += s.evalSolution_R_GSA(solution, verbose, scenarioHorizon);

		return eval / scenarioPool.size();
	}
};






template <class Scenario, class Solution> 
class VRP_ScenarioPool_Volatile {
protected:
	const VRP_instance& I;
	int numberScenarios;
	VRP_type sol_type;
	int currentTime = -1;
private:
	const Solution* solution_hProbas;
	vector<vector<vector<double>>> hProbas_Q;		// for debugging

	// vector<vector<vector<double>>> h_Qplus_w, g1_Qplus_w, g2_Qplus_w;
	// vector<vector<vector<vector<double>>>> h_Qplus, g1_Qplus, g2_Qplus;

public:
	friend class Scenario_SS_VRPTW_CR;

	VRP_ScenarioPool_Volatile(const VRP_instance& instance, int nb_scenarios, VRP_type vrp_type, int currentTime = -1) : I(instance) {
		numberScenarios = nb_scenarios;
		sol_type = vrp_type;
		this->currentTime = currentTime;
	}

	double computeExperimentalExpectedCost(const Solution& solution, RecourseStrategy strategy, bool debug=false, bool verbose = false, int scenarioHorizon = numeric_limits<int>::max()) {
		ASSERT(scenarioHorizon == numeric_limits<int>::max(), "not implemented yet");
		double eval = 0.0;
		// int Q = I.getVehicleCapacity(), H = I.getHorizon();

		solution_hProbas = & solution;

		int capacity = solution.getVehicleTypeAtRoute(1).getCapacity();
		hProbas_Q.resize(I.getNumberPotentialRequests() + 1);	
		for (auto& vec : hProbas_Q) {
			vec.resize(I.getHorizon() + 1);	
			for (auto& vec_ : vec)
				vec_.assign(capacity +1, 0.0);	
		}
		// h_Qplus_w.resize(I.getNumberPotentialRequests() + 1);	
		// for (auto& vec : h_Qplus_w) {
		// 	vec.resize(H + 1);	
		// 	for (auto& vec_ : vec)
		// 		vec_.assign(Q + 1, 0.0);
		// }
		// h_Qplus.resize(I.getNumberPotentialRequests() + 1);	
		// for (auto& vec : h_Qplus) {
		// 	vec.resize(H + 1);	
		// 	for (auto& vec_ : vec) {
		// 		vec_.resize(Q + 1);
		// 		for (auto& vec__ : vec_)
		// 			vec__.assign(I.getNumberPotentialRequests() + 1, 0.0);	
		// 	}
		// }
			

		// Scenario s(I, this);
		Scenario s(I, sol_type, currentTime);
		for (int i=0; i<numberScenarios; i++) {
			eval += s.evalSolution(solution, strategy, debug, verbose, scenarioHorizon);
			s.reSampleScenario(currentTime);
		}

		return eval / numberScenarios;
	}



	// string& toString_hProbas() {
	// 	ostringstream out; 
	// 	out.setf(std::ios::fixed);
	// 	out.precision(2);

	// 	const Solution& solution = *solution_hProbas;
	// 	const vector< vector< vector<VRP_request>>> & requests_ordered_per_waiting_locations = solution.getRequestAssignment(); 
		
	// 	for (int route=1; route <= solution.getNumberRoutes(); route++) {
	// 		out << outputMisc::greenExpr(true) << "Vehicle " << route << outputMisc::resetColor() << "\t\t t_min: min. handle time fr/ waiting loc \t t_max: max. handle time fr/ waiting loc if fixed arrival times" << endl;

	// 		out << outputMisc::greenExpr(true) << "H:                                                                                                "; 
	// 		for (int t=1; t <= I.getHorizon(); t++) 
	// 			out << setw(3) << t << "   "; 
	// 		out << endl << "Req:" << endl << outputMisc::resetColor() ;

	// 		for (int pos=1; pos <= solution.getRouteSize(route)-1; pos++) {

	// 			VRP_solution::SolutionElement w; solution.getSolutionElementAtPosition(&w, route, pos);
	// 			const VRP_vertex& w_vertex = solution.getVertexAtElement(w);
	// 			auto& waiting_loc = requests_ordered_per_waiting_locations[route][pos];
	// 			for (auto it = waiting_loc.cbegin(); it != waiting_loc.cend(); it++) {		
	// 				auto& r = *it;
					
	// 				out << outputMisc::blueExpr(true) << w_vertex.toString() << outputMisc::resetColor();
	// 				out << outputMisc::greenExpr(true) << r.toString(true) << outputMisc::resetColor() << "  ";
	// 				out << outputMisc::blueExpr(true) << "tmin=" << setw(3) << (int) solution.t_min(r,w) << " tmax=" << setw(3) << (int) solution.t_max(r,w) << " S=" << setw(3) << (int) I.travelTime(w_vertex,r.getVertex()) + (int) r.duration + (int) I.travelTime(r.getVertex(), w_vertex) << outputMisc::resetColor() << "  ";
	// 				for (int t=1; t <= I.getHorizon(); t++) {
	// 					double h_t = 0.0;
						
	// 					// switch (strategy) {
	// 					// 	case R_INFTY:
	// 					// 	case R_CAPA:
	// 					// 		ASSERT((int)hProbas_Q[r.id][t].size() == I.getVehicleCapacity()+1, "");
	// 					// 		for (int q=0; q <= I.getVehicleCapacity(); q++) {
	// 					// 			h_t += hProbas_Q[r.id][t][q];
	// 					// 		}
	// 					// 		break;
	// 					// 	case R_CAPA_PLUS:
	// 					// 		for (int q=0; q <= I.getVehicleCapacity(); q++) {
	// 					// 			h_t += h_Qplus_w[r.id][t][q];
	// 					// 			for (auto& r_ : requests_ordered_per_waiting_locations[route][pos])	
	// 					// 				h_t += h_Qplus[r.id][t][q][r_.id];
	// 					// 		}
	// 					// 		break;
								
	// 					// 	default: _ASSERT_(false, "");
	// 					// }
	// 					// (void)strategy; // so it shuts up with unused warning
	// 					ASSERT((int)hProbas_Q[r.id][t].size() == I.getVehicleCapacity()+1, "");
	// 					for (int q=0; q <= I.getVehicleCapacity(); q++) {
	// 						h_t += hProbas_Q[r.id][t][q];
	// 					}
						
	// 					out << " "; if (h_t < 0.995) out << " ";
	// 					out << outputMisc::blueExpr(h_t > 0) << setfill('0') << setw(2) << (int)round(h_t * 100) << outputMisc::resetColor() << setfill(' ') << "  ";
						
	// 				}
	// 				out << endl;
	// 			}
	// 		} 
	// 		out << endl;
	// 	}
	// 	static string str = ""; str = out.str();
	// 	return (str);
	// }

	// string& toString_h_v(const Solution& solution, int route, int request_id, int n) {
	// 	ostringstream out; 
	// 	out.setf(std::ios::fixed);
	// 	out.precision(2);

	// 	const vector< vector< vector<VRP_request>>> & requests_ordered_per_waiting_locations = solution.getRequestAssignment(); 
		
	
	// 	out << outputMisc::greenExpr(true) << "Vehicle " << route << outputMisc::resetColor() << "\t\t t_min: min. handle time fr/ waiting loc \t t_max: max. handle time fr/ waiting loc if fixed arrival times" << endl;

	// 	out << outputMisc::greenExpr(true) << "H:                                                                               "; 
	// 	for (int t=1; t <= I.getHorizon(); t++) 
	// 		out << setw(3) << t << "   "; 
	// 	out << endl << "Req:" << endl << outputMisc::resetColor() ;

	// 	int n_ = 0;

	// 	for (int pos=1; pos <= solution.getRouteSize(route)-1; pos++) {

	// 		VRP_solution::SolutionElement w; solution.getSolutionElementAtPosition(&w, route, pos);
	// 		const VRP_vertex& w_vertex = solution.getVertexAtElement(w);
	// 		auto& waiting_loc = requests_ordered_per_waiting_locations[route][pos];
	// 		for (auto it = waiting_loc.cbegin(); it != waiting_loc.cend(); it++) {		
	// 			auto& r = *it;
				
	// 			if (r.id == request_id) 
	// 				n_ = n;

	// 			if (n_ > 0) {
	// 				out << outputMisc::blueExpr(true) << w_vertex.toString() << outputMisc::resetColor();
	// 				out << outputMisc::greenExpr(true) << r.toString(true) << outputMisc::resetColor() << "  ";
	// 				out << outputMisc::blueExpr(true) << "tmin=" << setw(3) << (int) solution.t_min(r,w) << " tmax=" << setw(3) << (int) solution.t_max(r,w) << " S=" << setw(3) << (int) I.travelTime(w_vertex,r.getVertex()) + (int) r.duration + (int) I.travelTime(r.getVertex(), w_vertex) << outputMisc::resetColor() << "  ";
	// 				for (int t=1; t <= I.getHorizon(); t++) {
	// 					double h_t = 0.0;
						
	// 					ASSERT((int)hProbas_Q[r.id][t].size() == I.getVehicleCapacity()+1, "");
	// 					for (int q=0; q <= I.getVehicleCapacity(); q++) 
	// 						h_t += hProbas_Q[r.id][t][q];	
						
	// 					out << " "; if (h_t < 0.995) out << " ";
	// 					out << outputMisc::blueExpr(h_t > 0) << setfill('0') << setw(2) << (int)round(h_t * 100) << outputMisc::resetColor() << setfill(' ') << "  ";	
	// 				}
	// 				out << endl;

	// 				double h_t_w = 0.0;
	// 				for (int t=1; t <= I.getHorizon(); t++) 
	// 					for (int q=0; q <= I.getVehicleCapacity(); q++)
	// 						h_t_w += h_Qplus_w[r.id][t][q];
	// 				if (h_t_w > 0) {
	// 					out << outputMisc::magentaExpr(true);
	// 					out << "\tFrom waiting vertex: \t\t\t\t\t\t\t";
	// 					for (int t=1; t <= I.getHorizon(); t++) {
	// 						h_t_w = 0.0;
	// 						for (int q=0; q <= I.getVehicleCapacity(); q++)
	// 							h_t_w += h_Qplus_w[r.id][t][q];
	// 						out << " "; if (h_t_w < 0.995) out << " ";
	// 						if (h_t_w > 0)
	// 							out << setfill('0') << setw(2) << (int)round(h_t_w * 100) << setfill(' ') << "  ";
	// 						else out << "    ";
	// 					}
	// 					out << outputMisc::resetColor() <<  endl;
	// 				}

	// 				for (int pos=1; pos <= solution.getRouteSize(route)-1; pos++) {
	// 					auto& waiting_loc = requests_ordered_per_waiting_locations[route][pos];
	// 					for (auto it = waiting_loc.cbegin(); it != waiting_loc.cend(); it++) {		
	// 						auto& r_ = *it;

	// 						double h_t_rid = 0.0;
	// 						for (int t=1; t <= I.getHorizon(); t++) 
	// 							for (int q=0; q <= I.getVehicleCapacity(); q++)
	// 								h_t_rid += h_Qplus[r.id][t][q][r_.id];
	// 						if (h_t_rid > 0) {
	// 							out << outputMisc::magentaExpr(true);
	// 							out << "\tFrom request " << r_.id << ": \t\t\t\t\t\t\t";
	// 							for (int t=1; t <= I.getHorizon(); t++) {
	// 								h_t_rid = 0.0;
	// 								for (int q=0; q <= I.getVehicleCapacity(); q++)
	// 									h_t_rid += h_Qplus[r.id][t][q][r_.id];
	// 								out << " "; if (h_t_rid < 0.995) out << " ";
	// 								if (h_t_rid > 0)
	// 									out << setfill('0') << setw(2) << (int)round(h_t_rid * 100) << setfill(' ') << "  ";
	// 								else out << "    ";
	// 							}
	// 							out << outputMisc::resetColor() <<  endl;
	// 						}
	// 					}
	// 				}

	// 				n_--;
	// 			}
	// 		}
	// 	}
	// 	static string str = ""; str = out.str();
	// 	return (str);
	// }

};





class Scenario_SS_DS_VRPTW_CR {
private:
	const VRP_instance& I;
	VRP_type sol_type;

	vector<vector<const VRP_request *>> sampled_requests;	// the sampled requests that constitute the scenario: sampled_requests[c][t]= r <==> the request *r appears at time unit t (not time slot !!) at customer vertex c ; otherwise = NULL
	vector<VRP_instance::RequestAttributes> sampled_requests_sequence;
	int sampled_time = -42;
	bool sampled(const VRP_request& r) const;				// based on sampled_requests, tells whether a particular request is revealed as present
	const VRP_request* sampled(int region, int time_unit) const;	// based on sampled_requests, tells whether a request occurs at particular customer region and time unit (if so returns a pointer to the request, otherwise NULL)

	double 	evalSolution_R_INFTY_CAPA(const Solution_SS_VRPTW_CR& solution, bool capacity, bool debug, bool verbose, int scenarioHorizon = numeric_limits<int>::max()) const;
	double 	evalSolution_R_CAPA_PLUS(const Solution_SS_VRPTW_CR& solution, bool debug, bool verbose, int scenarioHorizon = numeric_limits<int>::max()) const;
	double 	evalSolution_R_BASIC_INFTY_CAPA(const Solution_SS_VRPTW_CR& solution, bool capacity, bool debug, bool verbose, int scenarioHorizon = numeric_limits<int>::max()) const;

	Solution_DS_VRPTW* s_copy = nullptr;

	// VRP_ScenarioPool_Volatile<Scenario_SS_VRPTW_CR, Solution_SS_VRPTW_CR>* pool_volatile_SS = NULL;
	// VRP_ScenarioPool_Volatile<Scenario_SS_VRPTW_CR, Solution_SS_VRPTW_CR>* pool_volatile_DS = NULL;

public:

	// Scenario_SS_DS_VRPTW_CR(const VRP_instance& instance, VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR>* pool_SS = NULL, VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_DS_VRPTW_CR>* pool_DS = NULL);	// constructor ***
	Scenario_SS_DS_VRPTW_CR(const VRP_instance& instance, VRP_type vrp_type, int currentTime);	// constructor ***
	~Scenario_SS_DS_VRPTW_CR();	

	double 	evalSolution(const Solution_SS_VRPTW_CR& solution, RecourseStrategy strategy, bool debug = false, bool verbose = false, int scenarioHorizon = numeric_limits<int>::max());
	double 	evalSolution_R_GSA(const Solution_DS_VRPTW& solution, bool verbose, int scenarioHorizon = numeric_limits<int>::max());

	const vector<VRP_instance::RequestAttributes> & getRequestSequence() const { return sampled_requests_sequence; }

	void 	updateScenario(double currentTime);			// Updates the scenario wrt the realizations in the instance file and the current time
	void 	reSampleScenario(double currentTime);		// Re-sample the scenario (for instance, in case it is not valable anymore), according to current time

	std::string& toString() const;
	std::string& toString_perRegion() const;
};









#endif



